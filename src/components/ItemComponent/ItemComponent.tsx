import * as React from 'react';
import {
  Card, Grid, GridListTileBar, IconButton,
  Theme, Typography,
  withStyles,
  WithStyles,
} from '@material-ui/core';
import {
  StyleRules
} from '@material-ui/core/styles';
import {
  compose,
} from 'redux';
import {ISlot} from "../../interfaces/ISlot";
import {IItem} from "../../interfaces/IItem";
import {IMap} from "../../interfaces/IMap";

const styles = (theme: Theme): StyleRules => ({
  root: {},
  card: {
    height: '100%',
    backgroundColor: '#f9d342'
  },
  fullHeight: {
    height: '100%'
  },
});

interface IItemComponentComponentProps {
  items: IMap<IItem>
  slotValue: ISlot;
  slotId: string
  onClickRefillHandler(slotId: string, itemId: string): void
}

//from state
interface IItemComponentProps extends IItemComponentComponentProps {
}

type ItemComponentType = IItemComponentProps & WithStyles<keyof ReturnType<typeof styles>>;

const ItemComponent: React.FC<ItemComponentType> = ({classes, slotValue, slotId, items, onClickRefillHandler}) => {
  const subtitle = (price: number, pieces: number) => (
    <Grid>
      <Grid>{`${price}$`}</Grid>
      <Grid>{`Pieces: ${pieces}`}</Grid>
    </Grid>
  )

  const getItemPrice = (slotValue: ISlot): number => {
    const item: IItem = items[slotValue.id];
    const price = item && item.price;
    return price || 0;
  }

  return (
    <Card
      className={classes.card}
      raised={true}
    >
      <Grid
        container={true}
        direction='column'
      >
        {
          slotValue.id &&
          <Grid>
            <IconButton
              size='small'
              onClick={() => onClickRefillHandler(slotId, slotValue.id)}
            >
              <Typography
                variant='caption'
                style={{color: 'white'}}
              >
                Refill
              </Typography>
            </IconButton>
          </Grid>
        }
        <Grid
          style={{
            margin: '0 auto',
            marginTop: '8px'
          }}
        >
          <Typography
            variant='body2'
          >
            {slotValue.name.charAt(0).toUpperCase() + slotValue.name.slice(1)}
          </Typography>
        </Grid>
      </Grid>
      <GridListTileBar
        title={`${slotId}`}
        subtitle={subtitle(getItemPrice(slotValue), slotValue.pieces)}
      />
    </Card>
  );
}

export default compose<React.ComponentClass<IItemComponentComponentProps>>(
  withStyles(styles),
)(ItemComponent);