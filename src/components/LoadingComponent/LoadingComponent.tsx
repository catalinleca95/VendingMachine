import * as React from 'react';
import {
  Dialog, DialogTitle,
  Theme,
  withStyles,
  WithStyles,
} from '@material-ui/core';
import {
  StyleRules
} from '@material-ui/core/styles';
import {
  compose,
} from 'redux';

const styles = (theme: Theme): StyleRules => ({
  root: {}
});

interface ILoadingComponentComponentProps {
  text: string,
  loading: boolean
}

//from state
interface ILoadingComponentProps extends ILoadingComponentComponentProps {
}

type LoadingComponentType = ILoadingComponentProps & WithStyles<keyof ReturnType<typeof styles>>;

const LoadingComponent: React.FC<LoadingComponentType> = ({text, loading}) => (
  <Dialog
    open={loading}
    aria-labelledby="alert-dialog-title"
    aria-describedby="alert-dialog-description"
  >
    <DialogTitle id="alert-dialog-title">{text}</DialogTitle>
  </Dialog>
)

export default compose<React.ComponentClass<ILoadingComponentComponentProps>>(
  withStyles(styles),
)(LoadingComponent);