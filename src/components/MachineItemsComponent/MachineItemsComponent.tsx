import * as React from 'react';
import {
  GridList, GridListTile,
  Theme,
  withStyles,
  WithStyles,
} from '@material-ui/core';
import {
  StyleRules
} from '@material-ui/core/styles';
import {
  compose,
} from 'redux';
import {isObjectEmptyOrUndefined} from "../../utils";
import {ISlot} from "../../interfaces/ISlot";
import {IItem} from "../../interfaces/IItem";
import {IMap} from "../../interfaces/IMap";
import ItemComponent from "../ItemComponent/ItemComponent";

const styles = (theme: Theme): StyleRules => ({
  root: {},
  card: {
    height: '100%',
    backgroundColor: '#f9d342'
  },
  fullHeight: {
    height: '100%',
  },
});

interface IMachineItemsComponentComponentProps {
  slotMap: IMap<ISlot>
  items: IMap<IItem>
  onClickRefillHandler(slotId: string, itemId: string): void
}

//from state
interface IMachineItemsComponentProps extends IMachineItemsComponentComponentProps {
}

type MachineItemsComponentType = IMachineItemsComponentProps & WithStyles<keyof ReturnType<typeof styles>>;

const MachineItemsComponent: React.FC<MachineItemsComponentType> = ({classes, slotMap, items, onClickRefillHandler}) => (
  <GridList
    cellHeight='auto'
    cols={4}
    className={classes.fullHeight}
  >
    {
      !isObjectEmptyOrUndefined(slotMap) &&
      Object.keys(slotMap).map((slotId: string, index) => {
        const slotValue: ISlot = slotMap[slotId];
        return (
          <GridListTile key={`${index}${slotId}`}>
            <ItemComponent
              slotValue={slotValue}
              slotId={slotId}
              items={items}
              onClickRefillHandler={onClickRefillHandler}
            />
          </GridListTile>
        )
      })
    }
  </GridList>
)

export default compose<React.ComponentClass<IMachineItemsComponentComponentProps>>(
  withStyles(styles),
)(MachineItemsComponent);