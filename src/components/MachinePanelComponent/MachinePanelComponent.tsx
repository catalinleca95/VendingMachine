import * as React from 'react';
import {
  Button, FormHelperText,
  Grid, IconButton, InputAdornment, MenuItem, TextField,
  Theme, Typography,
  withStyles,
  WithStyles,
} from '@material-ui/core';
import {
  StyleRules
} from '@material-ui/core/styles';
import {
  compose,
} from 'redux';

const styles = (theme: Theme): StyleRules => ({
  root: {},
  panel: {
    width: 'calc(100% - 24px)',
    margin: '0 auto',
    height: '100%'
  },
  inputPanel: {
    backgroundColor: 'darkgrey',
    padding: '4px',
    borderRadius: '5px'
  },
  errorPanel: {
    height: '40px',
    margin: '0 auto'
  },
  centerItem: {
    margin: '0 auto'
  },
  buttonStyle: {
    color: "#f9d342"
  },
  menuItem: {
    backgroundColor: '#292826',
  },
  menuList: {
    padding: '0',
    color: '#f9d342',
    '&:hover': {
      backgroundColor: 'rgba(41,40,38, 0.7) !important',
    }
  }
});

interface IMachinePanelComponentComponentProps {
  currentMoney: number;
  money: number;
  code: string;
  panelError: string;
  getItemHandler(): void;
  addMoneyHandler(): void;
  clearCode(): void;
  handleNumPadClick(value: string): void;
  handleChangeCode(e: React.ChangeEvent<HTMLInputElement>): void;
  handleChangeBillValue(e: React.ChangeEvent<HTMLInputElement>): void;
}

//from state
interface IMachinePanelComponentProps extends IMachinePanelComponentComponentProps {
}

type MachinePanelComponentType = IMachinePanelComponentProps & WithStyles<keyof ReturnType<typeof styles>>;

const MachinePanelComponent: React.FC<MachinePanelComponentType> = ({
  classes, currentMoney, money, code, panelError, handleChangeBillValue, addMoneyHandler, handleNumPadClick, clearCode, handleChangeCode, getItemHandler
  }) => (
    <Grid
      container={true}
      direction='column'
      className={classes.panel}
      justify='space-evenly'
    >
      <Grid
        className={classes.errorPanel}
      >
        {panelError !== '' && <FormHelperText id='helper-text' error={true}>{panelError}!</FormHelperText>}
      </Grid>
      <Grid
        className={classes.inputPanel}
      >
        <Grid>
          <Typography variant='body1'>Money: {`${currentMoney}$`}</Typography>
        </Grid>
        <Grid>
          <TextField
            id="outlined-select-currency-native"
            select={true}
            label="Add Money"
            value={money}
            onChange={handleChangeBillValue}
            margin="normal"
            variant="outlined"
            fullWidth={true}
            InputProps={{
              startAdornment: <InputAdornment position="start">$</InputAdornment>,
            }}
            SelectProps={{
              MenuProps: {
                classes: {
                  list: classes.menuList
                }
              }
            }}
          >
            {[0.5, 1, 5, 10].map((value, index) => (
              <MenuItem key={`${index}${value}`} value={value} className={classes.menuItem}>
                {`${value}$`}
              </MenuItem>
            ))}
          </TextField>
        </Grid>
        <Grid>
          <Button
            size='small'
            onClick={addMoneyHandler}
            disabled={!money}
          >
            Add Money
          </Button>
        </Grid>
        <Grid
          container={true}
        >
          <TextField
            id="outlined-number"
            label='Product Code'
            value={code}
            InputLabelProps={{
              shrink: true,
            }}
            disabled={true}
            margin="normal"
            variant="outlined"
            InputProps={{
              startAdornment: <InputAdornment position="start">S</InputAdornment>,
              endAdornment: <InputAdornment position="end">
                <IconButton size='small' onClick={clearCode}>x</IconButton>
              </InputAdornment>,
            }}
          />
          {
            ['1','2','3','4','5','6','7','8','9','0'].map( (value: string, index: number) => (
              <Grid
                key={`${index}${value}`}
                item={true}
                xs={4}
                className={value === '0' ? classes.centerItem : ''}
              >
                <IconButton
                  className={classes.buttonStyle}
                  onClick={() => handleNumPadClick(value)}
                >
                  {value}
                </IconButton>
              </Grid>
            ))
          }
        </Grid>
        <Grid>
          <Button
            size='small'
            onClick={getItemHandler}
          >
            Get Item
          </Button>
        </Grid>
      </Grid>
    </Grid>
  );


export default compose<React.ComponentClass<IMachinePanelComponentComponentProps>>(
  withStyles(styles),
)(MachinePanelComponent);