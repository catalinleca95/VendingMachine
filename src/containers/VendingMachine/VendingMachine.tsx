import * as React from 'react';
import {
  Paper,
  Theme,
  Grid,
  withStyles,
  WithStyles,
} from '@material-ui/core';
import {
  StyleRules
} from '@material-ui/core/styles';
import {
  compose,
} from 'redux';
import {IAppData} from "../../interfaces/IAppData";
import {createStructuredSelector} from "reselect";
import {makeSelectAppStateMoney} from "../../store/selectors";
import {connect} from "react-redux";
import {ISlot} from "../../interfaces/ISlot";
import {IItem} from "../../interfaces/IItem";
import {IMap} from "../../interfaces/IMap";
import {FillSlotActionCreator, GetItemActionCreator} from "../../store/actionCreators";
import {AddMoneyAction} from "../../store/actions";
import withDelay from "../../hoc/withDelay/withDelay";
import MachinePanelComponent from "../../components/MachinePanelComponent/MachinePanelComponent";
import MachineItemsComponent from "../../components/MachineItemsComponent/MachineItemsComponent";
import {ThunkDispatch} from "redux-thunk";
import {IAction} from "../../interfaces/IAction";
import {IState} from "../../interfaces/IState";

const styles = (theme: Theme): StyleRules => ({
  root: {},
  paperClass: {
    marginBottom: '16px',
    width: '600px',
    height: '650px',
    backgroundColor: '#292826',
    padding: '4px'
  },
  card: {
    height: '100%',
    backgroundColor: '#f9d342'
  },
  fullHeight: {
    height: '100%'
  },
});

interface IVendingMachineComponentProps {
  appData: IAppData
}

//from state
interface IVendingMachineProps extends IVendingMachineComponentProps {
  currentMoney: number
  loadingComponent: React.FC;
  addMoney(value: number): void
  getItem(slotId: string): void
  onStartLoading(text: string): void;
  refillSlot(slotId: string, itemId: string): void;
}

interface IVendingMachineState {
  money: number;
  code: string;
  panelError: string;
}

type VendingMachineType = IVendingMachineProps & WithStyles<keyof ReturnType<typeof styles>>;

class VendingMachine extends React.Component<VendingMachineType, {}> {
  public state: IVendingMachineState = {
    money: 0,
    code: '',
    panelError: '',
  }

  public handleChangeBillValue = (e: React.ChangeEvent<HTMLInputElement>) => {
    const value: number = +e.target.value;
    this.setState({money: value})
  }

  public handleChangeCode = (e: React.ChangeEvent<HTMLInputElement>) => {
    const value: string = e.target.value;
    this.setState({code: value})
  }

  public handleSetPanelError = (error: string) => {
    this.setState({panelError: error})
  }

  public onClickRefillHandler = (slotId: string, itemId: string) => {
    this.props.refillSlot(slotId, itemId)
  }

  public handleNumPadClick = (value: string) => {
    this.setState({
      code: this.state.code + value
    })
  }

  public clearCode = () => {
    this.setState({code: ''})
  }

  public addMoneyHandler = () => {
    const {
      money
    } = this.state;

    const {
      addMoney,
      onStartLoading
    } = this.props;

    onStartLoading('Adding Money...');
    addMoney(money)
  }

  public getItemHandler = () => {
    const {
      code
    } = this.state;

    const {
      appData,
      onStartLoading,
      getItem,
      currentMoney
    } = this.props;

    const slots: IMap<ISlot> = appData && appData.slotMap;
    const items: IMap<IItem> = appData && appData.items;

    const isSlot = Object.keys(slots).find( (slotId: string) => slotId === `S${code}`)

    if (!isSlot) {
      this.handleSetPanelError('Slot Number Invalid')
    } else {
      const isSlotLoaded = slots[isSlot].pieces;
      if (isSlotLoaded) {
        const itemSlotId = slots[isSlot].id
        const itemPrice = items[itemSlotId].price;

        if (currentMoney >= itemPrice) {
          this.handleSetPanelError('')
          this.clearCode()
          onStartLoading('Getting Item...');
          getItem(isSlot)
        } else {
          this.handleSetPanelError('Not Enough Money')
        }
      } else {
        this.handleSetPanelError('The Slot Is Empty. Don\'t loose your money')
      }
    }

  }

  render() {
    const {
      classes,
      appData,
      currentMoney,
      loadingComponent
    } = this.props;

    const {
      money,
      code,
      panelError,
    } = this.state;

    const slotMap: IMap<ISlot> = appData && appData.slotMap;
    const items: IMap<IItem> = appData && appData.items

    return (
      <Grid>
        {loadingComponent}
        <Paper
          className={classes.paperClass}
        >
          <Grid
            container={true}
            direction='row'
            alignItems='center'
            className={classes.fullHeight}
          >
            <Grid
              item={true}
              xs={9}
              className={classes.fullHeight}
            >
              <MachineItemsComponent
                items={items}
                slotMap={slotMap}
                onClickRefillHandler={this.onClickRefillHandler}
              />
            </Grid>
            <Grid
              item={true}
              xs={3}
              className={classes.fullHeight}
            >
              <MachinePanelComponent
                currentMoney={currentMoney}
                money={money}
                code={code}
                panelError={panelError}
                handleChangeBillValue={this.handleChangeBillValue}
                addMoneyHandler={this.addMoneyHandler}
                handleChangeCode={this.handleChangeCode}
                getItemHandler={this.getItemHandler}
                handleNumPadClick={this.handleNumPadClick}
                clearCode={this.clearCode}
              />
            </Grid>
          </Grid>
        </Paper>
      </Grid>
    );
  }
}

const mapStateToProps = (state: IState) => {
  return createStructuredSelector({
    currentMoney: makeSelectAppStateMoney()
  })(state);
}

const mapDispatchToProps = (dispatch: ThunkDispatch<IState, {}, IAction>) => {
  return {
    addMoney: (value: number) => dispatch(AddMoneyAction(value)),
    getItem: (slotId: string) => dispatch(GetItemActionCreator(slotId)),
    refillSlot: (slotId: string, itemId: string) => dispatch(FillSlotActionCreator(slotId, itemId))
  }
}
export default compose<React.ComponentClass<IVendingMachineComponentProps>>(
  connect(mapStateToProps, mapDispatchToProps),
  withStyles(styles),
  withDelay,
)(VendingMachine);