import * as React from 'react';
import {
  Button,
  Grid,
  Paper,
  Theme, Typography,
  withStyles,
  WithStyles,
} from '@material-ui/core';
import {
  StyleRules
} from '@material-ui/core/styles';
import {
  compose,
} from 'redux';
import {IAppData} from "../../interfaces/IAppData";
import {isObjectEmptyOrUndefined} from "../../utils";
import {connect} from "react-redux";
import {FillSlotActionCreator} from "../../store/actionCreators";
import {ThunkDispatch} from "redux-thunk";
import {IState} from "../../interfaces/IState";
import {IAction} from "../../interfaces/IAction";
import {IMap} from "../../interfaces/IMap";
import {ISlot} from "../../interfaces/ISlot";
import {IItem} from "../../interfaces/IItem";

const styles = (theme: Theme): StyleRules => ({
  root: {},
  addItemsContainer: {
    width: '200px',
    backgroundColor: '#292826',
  },
  scrollContainer: {
    overflowY: 'auto',
    height: '500px',
  },
  titleContainer: {
    padding: '4px',
    color: '#f9d342'
  },
  buttonStyle: {
    backgroundColor: '#f9d342',
    color: 'grey'
  }
});

interface IAddItemsComponentComponentProps {
  appData: IAppData
}

//from state
interface IAddItemsComponentProps extends IAddItemsComponentComponentProps {
  fillItem(slotId: string, itemId: string): void
}

type AddItemsComponentType = IAddItemsComponentProps & WithStyles<keyof ReturnType<typeof styles>>;

class AddItemsComponent extends React.PureComponent<AddItemsComponentType, {}> {
  render() {
    const {
      appData,
      fillItem,
      classes
    } = this.props;

    const items: IMap<IItem> = appData && appData.items;
    const slotMap: IMap<ISlot> = appData && appData.slotMap;

    const handleClickItem = (itemId: string) => {
      const firstEmptySlot = !isObjectEmptyOrUndefined(slotMap) &&
        Object.keys(slotMap).find( slotId => {
          const slotValue = slotMap[slotId];
          return slotValue.pieces === 0 ? slotId : false
        })

      firstEmptySlot && fillItem(firstEmptySlot as string, itemId)
    }

    return (
      <Grid>
        <Paper
          className={classes.addItemsContainer}
        >
          <Grid
            className={classes.titleContainer}
          >
            <Typography variant='body1'>
              Click one item to fill the first empty spot in the vending machine
            </Typography>
          </Grid>
          <Grid
            container={true}
            direction='column'
            wrap='nowrap'
            className={classes.scrollContainer}
          >
            {
              !isObjectEmptyOrUndefined(items) &&
              Object.keys(items).map( (value, index) => {
                const itemValue = items[value];
                return (
                  <Grid
                    key={`${index}${value}`}
                    item={true}
                  >
                    <Button
                      fullWidth={true}
                      variant='outlined'
                      onClick={() => handleClickItem(value)}
                      className={classes.buttonStyle}
                    >
                      {itemValue.name}
                    </Button>
                  </Grid>
                )
              })
            }
          </Grid>
        </Paper>
      </Grid>
    );
  }
}

const mapDispatchToProps = (dispatch: ThunkDispatch<IState, {}, IAction>) => {
  return {
    fillItem: (slotId: string, itemId: string) => dispatch(FillSlotActionCreator(slotId, itemId))
  }
}

export default compose<React.ComponentClass<IAddItemsComponentComponentProps>>(
  connect(null, mapDispatchToProps),
  withStyles(styles),
)(AddItemsComponent);