import * as React from 'react';
import {
  Grid,
  Theme, Typography,
  withStyles,
  WithStyles,
} from '@material-ui/core';
import {
  StyleRules
} from '@material-ui/core/styles';
import {
  compose,
} from 'redux';
import withData from "../../hoc/withData/withData";
import VendingMachine from "../VendingMachine/VendingMachine";
import AddItemsComponent from "../AddItemsComponent/AddItemsComponent";
import {IAppData} from "../../interfaces/IAppData";
import {IAppState} from "../../interfaces/IAppState";

const styles = (theme: Theme): StyleRules => ({
  root: {},
  container: {
    paddingTop: '24px',
    background: 'linear-gradient(45deg, grey, #f9d342)'
  }
});

interface IBodyComponentComponentProps {
}

//from state
interface IBodyComponentProps extends IBodyComponentComponentProps {
  appData: IAppData,
  appState: IAppState
}

type BodyComponentType = IBodyComponentProps & WithStyles<keyof ReturnType<typeof styles>>;

const BodyComponent: React.FC<BodyComponentType> = ({classes, appData}) => (
  <Grid
    container={true}
    direction='row'
    justify='space-evenly'
    alignItems='flex-start'
    className={classes.container}
  >
    <Grid
      style={{marginBottom: '20px'}}
    >
      <Typography variant='h3'>Vending Machine</Typography>
    </Grid>
    <Grid
      container={true}
      item={true}
      direction='row'
      alignItems='flex-start'
      justify='space-around'
    >
      <VendingMachine
        appData={appData}
      />
      <AddItemsComponent
        appData={appData}
      />
    </Grid>
  </Grid>
);

export default compose<React.ComponentClass<IBodyComponentComponentProps>>(
  withStyles(styles),
  withData,
)(BodyComponent);