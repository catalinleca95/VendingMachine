export const isObjectEmptyOrUndefined = (obj: any) => {
  if (!obj) {
    return true
  } else {
    return obj && Object.entries(obj).length === 0 && obj.constructor === Object
  }
}
