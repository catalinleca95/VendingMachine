export interface ISlot {
  id: string,
  name: string,
  pieces: number
}

export type SlotProps = keyof ISlot;