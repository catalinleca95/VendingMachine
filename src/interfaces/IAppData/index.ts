import {IItem} from "../IItem";
import {IMap} from "../IMap";
import {ISlot} from "../ISlot";

export interface IAppData {
  items: IMap<IItem>,
  slotMap: IMap<ISlot>
}

export type AppDataKeys = keyof IAppData;