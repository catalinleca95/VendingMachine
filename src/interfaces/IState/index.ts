import {IAppData} from "../IAppData";
import {IAppState} from "../IAppState";

export interface IState {
  appData: IAppData,
  appState: IAppState
}

export type StateKeys = keyof IState;