export interface IItem {
  name: string,
  price: number,
}

export type ItemKeys = keyof IItem;