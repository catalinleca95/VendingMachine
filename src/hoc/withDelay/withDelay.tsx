import * as React from 'react';
import {compose} from "redux";
import LoadingComponent from "../../components/LoadingComponent/LoadingComponent";

interface IWithDelayComponentProps {
  onStartLoading(): void
}

interface IWithDelayProps extends IWithDelayComponentProps {
  classes: any
  onStopLoading(): void
}

interface IWithDelayState {
  loading: boolean;
  text: string
}

const withDelay = <T extends IWithDelayComponentProps>(Component: React.ComponentType<T>) => {
  class WithDelay extends React.Component<T & IWithDelayProps> {
    public state: IWithDelayState = {
      loading: false,
      text: ''
    }

    /** Only a setTimeout. Its purpose is to imitate
     *  the machine behaviour when inserting a bill
     */
    public onStartLoading = (text: string) => {
      this.setState({
        text,
        loading: true
      })
      setTimeout( () => this.onStopLoading(), 1000)
    }

    public onStopLoading = () => {
      this.setState({loading: false})
    }

    render() {
      const {
        loading,
        text
      } = this.state;

      const loadingComponent = <LoadingComponent text={text} loading={loading}/>

      return  <Component
        {...this.props}
        onStartLoading={this.onStartLoading}
        loadingComponent={loadingComponent}
      />
    }
  }

  return compose<React.ComponentClass<IWithDelayProps>>(
  )(WithDelay)
}

export default withDelay;