import * as React from 'react';
import {IAppData} from "../../interfaces/IAppData";
import {IAction} from "../../interfaces/IAction";
import {compose, Dispatch} from "redux";
import {connect} from "react-redux";
import {InitStateActionCreator} from "../../store/actionCreators";
import {IAppState} from "../../interfaces/IAppState";
import {ThunkDispatch} from "redux-thunk";
import {IState} from "../../interfaces/IState";

interface IWithDataComponentProps {
}

//from state
interface IWithDataProps extends IWithDataComponentProps {
  appData: IAppData
  appState: IAppState
  dispatch: Dispatch<IAction>,
  initState(): void;
}

const withData = <T extends object>(Component: React.ComponentType<T>) => {
  class WithData extends React.Component<T & IWithDataProps> {
    componentDidMount() {
      this.props.initState()
    }

    render() {
      const {
        appData,
        appState
      } = this.props;

      return <Component {...this.props} appData={appData} appState={appState}/>
    }
  }

  const mapStateToProps = (state: IState) => ({
    appData: state.appData,
    appState: state.appState
  })

  const mapDispatchToProps = (dispatch: ThunkDispatch<IState, {}, IAction>) => {
    return {
      initState: () => dispatch(InitStateActionCreator())
    }
  }

  return compose<React.ComponentClass<IWithDataProps>>(
    connect(mapStateToProps, mapDispatchToProps)
  )(WithData)
}

export default withData