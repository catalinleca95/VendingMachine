import * as React from 'react';
import './App.css';
import BodyComponent from "./containers/BodyComponent/BodyComponent";

const App: React.FC = () => {
  return (
    <React.Fragment>
      <BodyComponent/>
    </React.Fragment>
  );
}

export default App;
