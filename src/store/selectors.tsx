import {createSelector} from "reselect";
import {IAppData} from "../interfaces/IAppData";
import {IAppState} from "../interfaces/IAppState";
import {IItem} from "../interfaces/IItem";
import {IMap} from "../interfaces/IMap";
import {ISlot} from "../interfaces/ISlot";
import {IState} from "../interfaces/IState";

let initTime: any;

export const selectReducerState = () => (state: any) => {
  if (state != null) {
    return state;
  }
  const now = Date.now();
  if (initTime == null) {
    initTime = now;
  }

  if (now - initTime > 5000) {
    console.warn(`Missing Reducer State, have you added the Module to your application?`, { // tslint:disable-line
      state,
    });
  }
  return {};
};

export const makeSelectAppState = () => createSelector(
  selectReducerState(),
  (state: IState) => state.appState || {}
)

export const makeSelectAppStateMoney = () => createSelector(
  makeSelectAppState(),
  (state: IAppState) => state.currentMoney
)

export const makeSelectAppData = () => createSelector(
  selectReducerState(),
  (state: IState) => state.appData || {}
)

export const makeSelectAppDataItems = () => createSelector(
  makeSelectAppData(),
  (appData: IAppData) => appData.items || {}
)

export const makeSelectAppDataSlotMap = () => createSelector(
  makeSelectAppData(),
  (appData: IAppData) => appData.slotMap || {}
)

export const makeSelectItemById = (itemId: string) => createSelector(
  makeSelectAppDataItems(),
  (items: IMap<IItem>): IItem => items[itemId]
)

export const makeSelectSlotById = (slotId: string) => createSelector(
  makeSelectAppDataSlotMap(),
  (slotMap: IMap<ISlot>): ISlot => slotMap[slotId]
)

export default {
  selectReducerState
}

