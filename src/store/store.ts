import thunk from "redux-thunk";
import {applyMiddleware, combineReducers, compose, createStore} from "redux";
import appData from "./reducers/appData";
import appState from "./reducers/appState";

export default function configureStore() {
  const middlewares = [
    thunk,
  ]

  const enhancers= [
    applyMiddleware(...middlewares)
  ]

  const composeEnhancers =
    typeof window === 'object' &&
      (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
      ? (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
      : compose

  const combinedReducers = combineReducers({
    appData,
    appState
  })

  return createStore(
    combinedReducers,
    composeEnhancers(...enhancers)
  )
}