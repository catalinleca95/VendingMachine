import {Dispatch} from "redux";
import {IAction} from "../interfaces/IAction";
import {IAppData} from "../interfaces/IAppData";
import axios, {AxiosResponse} from 'axios';
import {AddMoneyAction, EmptySlotAction, ErrorAction, FillSlotAction, GetItemAction, InitStateAction} from "./actions";
import {baseUrl, getFirebaseUrl, slotMapUrl} from "../constants/constants";
import {makeSelectItemById, makeSelectSlotById} from "./selectors";
import {IItem} from "../interfaces/IItem";
import {ISlot} from "../interfaces/ISlot";
import {IState} from "../interfaces/IState";
import {ThunkDispatch} from "redux-thunk";

export const InitStateActionCreator = () => async (dispatch: Dispatch<IAction>) => {
  try {
    const response: AxiosResponse = await axios.get(getFirebaseUrl(baseUrl));
    const data: IAppData = response.data;
    dispatch(InitStateAction(data))
  }
  catch (error) {
    console.error('error: ', error)
    dispatch(ErrorAction(error))
  }
}

export const FillSlotActionCreator = (slotId: string, itemId: string) => async (dispatch: Dispatch<IAction>, getState: () => IState) => {
  const state = getState();
  const item: IItem = makeSelectItemById(itemId)(state);
  const newSlotValue: ISlot = {
    id: itemId,
    name: item && item.name,
    pieces: 8
  }

  try {
    const response: AxiosResponse = await axios.patch(getFirebaseUrl(slotMapUrl, slotId), newSlotValue)
    dispatch(FillSlotAction(slotId, newSlotValue))
  } catch (error) {
    console.error('error: ', error);
    dispatch(ErrorAction(error))
  }
}

export const GetItemActionCreator = (slotId: string) => async (dispatch: ThunkDispatch<IState, {}, IAction>, getState: () => IState) => {
  const state = getState();
  const currentSlot: ISlot = makeSelectSlotById(slotId)(state);
  const slotItemId: string = currentSlot.id;
  const item: IItem = makeSelectItemById(slotItemId)(state);
  const itemPrice: number = item.price;
  const newSlot: ISlot = {
    ...currentSlot,
    pieces: currentSlot.pieces - 1
  }
  try {
    const response: AxiosResponse = await axios.patch(getFirebaseUrl(slotMapUrl, slotId), newSlot)
    const newSlotPieces = response.data.pieces;

    if (newSlotPieces === 0) {
      dispatch(EmptySlotActionCreator(slotId))
    }
    dispatch(GetItemAction(slotId))
    dispatch(AddMoneyAction(0 - +itemPrice))
  } catch (error) {
    console.error('error: ', error);
    dispatch(ErrorAction(error))
  }
}

export const EmptySlotActionCreator = (slotId: string) => async (dispatch: Dispatch<IAction>) => {
  const emptySlot: ISlot = {id: '', name: '', pieces: 0};
  try {
    const response: AxiosResponse = await axios.patch(getFirebaseUrl(slotMapUrl, slotId), emptySlot)
    dispatch(EmptySlotAction(slotId))
  } catch (error) {
    console.error('error: ', error);
    dispatch(ErrorAction(error))
  }
}
