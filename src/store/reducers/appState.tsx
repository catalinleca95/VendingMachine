import {IAction} from "../../interfaces/IAction";
import {IAppState} from "../../interfaces/IAppState";
import {ActionTypes} from "../actions";

const initState: IAppState = {
  currentMoney: 0
};

const appState = (state = initState, action: IAction) => {
  switch(action.type) {
    case ActionTypes.ADD_MONEY: {
      const {
        payload
      } = action;

      return {
        currentMoney: state.currentMoney + payload
      }
    }
    default:
      return state
  }
}

export default appState;