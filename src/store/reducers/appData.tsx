import {IAppData} from "../../interfaces/IAppData";
import {IAction} from "../../interfaces/IAction";
import {ActionTypes} from "../actions";
import _ from 'lodash';
import {ISlot} from "../../interfaces/ISlot";

const initState: IAppData = {
  items: {},
  slotMap: {}
};

const appData = (state = initState, action: IAction) => {
  switch(action.type) {
    case ActionTypes.INIT_STATE: {
      const {
        payload
      } = action;

      return {
        ...payload
      }
    }
    case ActionTypes.ERROR: {
      const {
        payload
      } = action;

      return {
        ...state,
        error: {...payload}
      }
    }
    case ActionTypes.FILL_ITEM_ACTION: {
      const {
        payload: {
          slotId,
          item
        }
      } = action;

      const newState = _.cloneDeep(state)
      newState.slotMap[slotId] = {...item}

      return newState;
    }
    case ActionTypes.GET_ITEM: {
      const {
        payload: slotId
      } = action;

      const newState = _.cloneDeep(state);
      const slot: ISlot = newState.slotMap[slotId];

      if (!slot.pieces) {
        return state;
      } else {
        slot.pieces--
      }

      return newState;

    }
    case ActionTypes.EMPTY_SLOT: {
      const {
        payload: slotId
      } = action;

      const newState = _.cloneDeep(state);
      const emptySlot: ISlot = {id: '', name: '', pieces: 0}

      newState.slotMap[slotId] = emptySlot;
      return newState;
    }
    default:
      return state
  }
}

export default appData;