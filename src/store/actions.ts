import {IAction} from "../interfaces/IAction";
import {IAppData} from "../interfaces/IAppData";
import {ISlot} from "../interfaces/ISlot";

export enum ActionTypes {
  ERROR = 'ERROR',
  INIT_STATE = 'INIT_STATE',
  FILL_ITEM_ACTION = 'FILL_ITEM_ACTION',
  ADD_MONEY = 'ADD_MONEY',
  GET_ITEM = 'GET_ITEM',
  EMPTY_SLOT = 'EMPTY_SLOT'
}

export const InitStateAction = (data: IAppData): IAction => ({
  type:  ActionTypes.INIT_STATE,
  payload: data
})

export const ErrorAction = (error: Error): IAction => ({
  type: ActionTypes.ERROR,
  payload: error
})

export const FillSlotAction = (slotId: string, item: ISlot): IAction => ({
  type: ActionTypes.FILL_ITEM_ACTION,
  payload: {
    slotId,
    item
  }
})

export const AddMoneyAction = (value: number) => ({
  type: ActionTypes.ADD_MONEY,
  payload: value
})

export const GetItemAction = (slotId: string): IAction => ({
  type: ActionTypes.GET_ITEM,
  payload: slotId
})

export const EmptySlotAction = (slotId: string): IAction => ({
  type: ActionTypes.EMPTY_SLOT,
  payload: slotId
})