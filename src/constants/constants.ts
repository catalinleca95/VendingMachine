
const firebaseExtension = '.json';
export const baseUrl = '';
export const itemsUrl = 'items';
export const slotMapUrl = 'slotMap';

export const getFirebaseUrl = (...pathArguments: string[]): string => {
  return '/' + pathArguments.join('/') + firebaseExtension
}